﻿@extends('admin.master')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="portlet">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-users"></i> {{ $title }} <strong>({{ number_format($items->total()) }})</strong>
                    </div>
                </div>
                <div class="portlet-body">
                    <form action="{{ route('admin.{RouteName}.list') }}" method="GET" class="form-inline" role="form">
                        <div class="form-group">
                            <input type="text" name="created_at_start" id="created_at_start"
                                   value="{{ Request::get('created_at_start') }}"
                                   class="form-control input-small datetimepicker"
                                   data-timepicker="false"
                                   data-format="Y-m-d"
								   autocomplete="off"
                                   placeholder="Created from">
                        </div>

                        <div class="form-group">
                            <input type="text" name="created_at_end" id="created_at_end"
                                   value="{{ Request::get('created_at_end') }}"
                                   class="form-control input-small datetimepicker"
                                   data-timepicker="false"
                                   data-format="Y-m-d"
								   autocomplete="off"
                                   placeholder="To">
                        </div>

                        <div class="form-group">
                            <input type="text" class="form-control input-small" name="name" value="{{ Request::input('name') }}" placeholder="Tên">
                        </div>

                        <div class="row" style="margin-top: 10px">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <button type="submit" class="btn blue">{{ trans('action.search') }}</button>
                                    <a href="{{ route('admin.{RouteName}.list') }}" class="btn yellow">{{ trans('action.reset') }}</a>
                                </div>
                            </div>
                        </div>
                    </form>

                    <form action="{{ route('admin.{RouteName}.list') }}" method="POST" class="form-data">
                        {!! csrf_field() !!}
						
						{{--<div class="row" style="margin-bottom: 10px">--}}
                            {{--<div class="col-md-12">--}}
                                {{--<div class="btn-group pull-right"><a class="btn btn-default export-tool" href="javascript:void(0);">Excel</a></div>--}}
                            {{--</div>--}}
                        {{--</div>--}}

                        <div class="table-scrollable">
                            <table class="table table-striped table-bordered table-advance table-hover table_has_checkbox tableSorter">
                                <thead>
                                    <tr>
                                        <th class="table-checkbox nosort">
                                            <input type="checkbox" class="group-checkable" data-set=".table_has_checkbox .checkboxes"/>
                                        </th>
                                        <th class="{{ class_url_order('image') }}">
                                            <a href="{{ route('admin.{RouteName}.list', url_order('image')) }}">
                                                Hình đại diện
                                            </a>
                                        </th>
                                        <th class="{{ class_url_order('name') }}">
                                            <a href="{{ route('admin.{RouteName}.list', url_order('name')) }}">
                                                Tên
                                            </a>
                                        </th>
										{{--<th class="{{ class_url_order('sort_order') }}">--}}
                                            {{--<a href="{{ route('admin.{RouteName}.list', url_order('sort_order')) }}">--}}
                                                {{--Thứ tự hiển thị--}}
                                            {{--</a>--}}
                                        {{--</th>--}}
										<th class="{{ class_url_order('created_at') }}">
                                            <a href="{{ route('admin.{RouteName}.list', url_order('created_at')) }}">
                                                Ngày tạo
                                            </a>
                                        </th>
                                        <th class="nosort"></th>
                                    </tr>
                                </thead>
                                <tbody id="table-sortable">
                                    @foreach($items as $item)
                                        <tr>
											<input type="hidden" name="sort[]" value="{{ $item->sort_order }}">
											<input type="hidden" name="item_ids[]" value="{{ $item->id }}">
                                            <td><input type="checkbox" class="checkboxes" name="ids[]" value="{{ $item->id }}"/></td>
                                            <td><img width="100" height="100" src="{{ static_file($item->thumb('image', 'S')) }}"></td>
                                            <td>{{ $item->name }}</td>
											{{--<td class="column-sorter"><span class="fa fa-sort handle-sorter"></span></td>--}}
                                            <td>{{ $item->created_at->format('d/m/Y H:i:s') }}</td>
                                            <td class="text-right">
                                                @if(user_can('admin.{RouteName}.edit'))
                                                    <a href="{{ route('admin.{RouteName}.edit', $item->id) }}" class="btn default btn-xs green">
                                                        <i class="fa fa-edit"></i> {{ trans('action.edit') }}
                                                    </a>
                                                @endif

                                                @if(user_can('admin.{RouteName}.delete'))
                                                    <a class="btn default btn-xs red post-delete" data-message="{{ trans('action.delete_confirm') }}" href="{{ route('admin.{RouteName}.delete', [$item->id, '_token' => csrf_token()]) }}">
                                                        <i class="fa fa-times"></i> {{ trans('action.delete') }}
                                                    </a>
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </form>

                    <div class="text-right">
                        {!! $items->appends($paginate_data)->render() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection