@extends('frontend.master')

@section('title', $title)

@section('content')
    <div class="row">
        <div class="col-sm-12">
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-plus"></i> {{ $title }}
                    </div>
                </div>
                <div class="portlet-body form">
                    <form id="editPage" role="form" method="POST" class="form-horizontal" action="{{ route('admin.{RouteName}.edit', $item->id) }}">
                        {{ csrf_field() }}

                        <div class="form-body">
                            <div class="form-group">
                                <label class="control-label col-md-3">Title<span class="required">*</span></label>
                                <div class="col-md-9">
                                    <input type="text" name="title" value="{{ old('title', $item->title) }}"
                                           class="form-control" placeholder="Title">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Represent Image<span class="required">*</span></label>
                                <div class="col-md-4">
                                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                        @if ($item->image)
                                            <div class="fileinput-new thumbnail">
                                                <img src="{{ static_file($item->thumb('image', 'S')) }}"/>
                                            </div>
                                        @endif
                                        <div class="fileinput-preview fileinput-exists thumbnail"></div>
                                        <div>
                                            <span class="btn default btn-file">
                                                <span class="fileinput-new">{{ trans('action.pick_image') }}</span>
                                                <span class="fileinput-exists">{{ trans('action.change_image') }}</span>
                                                <input type="file" name="image">
                                            </span>
                                            <a href="#" class="btn red fileinput-exists" data-dismiss="fileinput">{{ trans('action.delete') }}</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3">Content<span class="required">*</span></label>
                                <div class="col-md-9">
                                    <textarea name="content" class="form-control ckeditor" id="ckeditor">{{ old('content', $item->content) }}</textarea>
                                </div>
                            </div>

                            <hr>
                            <div class="form-group">
                                <label class="control-label col-md-3"><h3>SEO</h3></label>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3">Meta Keyword</label>
                                <div class="col-md-9">
                                    <input type="text" name="meta_keywords" value="{{ old('meta_keywords', $item->meta_keywords) }}"
                                           class="form-control" placeholder="Meta Keywords">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3">Meta Description</label>
                                <div class="col-md-9">
                                    <input type="text" name="meta_description"
                                           value="{{ old('meta_description', $item->meta_description) }}" class="form-control"
                                           placeholder="Meta Description">
                                </div>
                            </div>
                        </div>

                        <div class="form-actions right">
                            <button type="submit" class="btn green">{{ trans('action.save') }}</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection