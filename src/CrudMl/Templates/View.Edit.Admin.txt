@extends('admin.master')

@section('content')
    <div class="row">
        <div class="col-sm-12">
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-plus"></i> {{ $title }}
                    </div>
                </div>
                <div class="portlet-body form">
                    <form id="editPage" role="form" method="POST" class="form-horizontal" action="{{ route('admin.{RouteName}.edit', $item->id) }}">
                        {{ csrf_field() }}

                        <div class="form-actions right">
                            <button type="submit" class="btn green">{{ trans('action.save') }}</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection