<?php

namespace Nguyen930411\Crud\Commands;

use Illuminate\Console\Command;
use File;

class CRUDMlCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:crud-ml {ModelName}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Make CRUD Multiple Language Eloquent';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        /** 
         * Init Model
         */
        $curPath = dirname(__DIR__) . '\CrudMl';
        $appPath = app_path();
        $resourcePath = resource_path();
        $model = ucfirst($this->argument('ModelName'));
		$model_trans = ucfirst($this->argument('ModelName')) . 'Trans';
        $controller_name = $model . 'Controller';
        $prefix = str_replace('_', '-', snake_case($model));
		$relation_id = snake_case($model) . '_id';
        /**
         * Paths
         */
        // Model
        $template_path_model = $curPath . '\Templates\\' . 'Model.txt';
        $model_path = 'Models\\' . $model;
        $model_full_path = $appPath . '\\' . $model_path . '.php';
		
		// Trans Model
        $template_path_model_trans = $curPath . '\Templates\\' . 'ModelTrans.txt';
        $model_trans_path = 'Models\\' . $model_trans;
        $model_trans_full_path = $appPath . '\\' . $model_trans_path . '.php';

        // Request
        $template_path_request_admin = $curPath . '\Templates\\' . 'Request.Admin.txt';
        $template_path_request_frontend = $curPath . '\Templates\\' . 'Request.Frontend.txt';
        $request_base_path_admin = $appPath . '\Http\Requests\Admin\\';
        $request_base_path_frontend = $appPath . '\Http\Requests\Frontend\\';
        $request_full_path_admin = $request_base_path_admin . $model . 'Request.php';
        $request_full_path_frontend = $request_base_path_frontend . $model . 'Request.php';

        // Controller
        $template_path_controller_admin = $curPath . '\Templates\\' . 'Controller.Admin.txt';
        $template_path_controller_frontend = $curPath . '\Templates\\' . 'Controller.Frontend.txt';
        $controller_base_path_admin = $appPath . '\Http\Controllers\Admin\\';
        $controller_base_path_frontend = $appPath . '\Http\Controllers\Frontend\\';
        $controller_full_path_admin = $controller_base_path_admin . $model . 'Controller.php';
        $controller_full_path_frontend = $controller_base_path_frontend . $model . 'Controller.php';

        // View
        $template_path_view_add_admin = $curPath . '\Templates\\' . 'View.Add.Admin.txt';
        $template_path_view_edit_admin = $curPath . '\Templates\\' . 'View.Edit.Admin.txt';
        $template_path_view_list_admin = $curPath . '\Templates\\' . 'View.List.Admin.txt';
        $template_path_view_add_frontend = $curPath . '\Templates\\' . 'View.Add.Frontend.txt';
        $template_path_view_edit_frontend = $curPath . '\Templates\\' . 'View.Edit.Frontend.txt';
        $template_path_view_list_frontend = $curPath . '\Templates\\' . 'View.List.Frontend.txt';
        $view_base_path_admin = $resourcePath . '\views\admin\\' . $prefix . '\\';
        $view_base_path_frontend = $resourcePath . '\views\frontend\\' . $prefix . '\\';
        $view_add_full_path_admin = $view_base_path_admin . 'add.blade.php';
        $view_edit_full_path_admin = $view_base_path_admin . 'edit.blade.php';
        $view_list_full_path_admin = $view_base_path_admin . 'list.blade.php';
        $view_add_full_path_frontend = $view_base_path_frontend . 'add.blade.php';
        $view_edit_full_path_frontend = $view_base_path_frontend . 'edit.blade.php';
        $view_list_full_path_frontend = $view_base_path_frontend . 'list.blade.php';

        /**
         * Table Name
         */
		$table_name = snake_case(str_plural($model));
		$table_trans_name = snake_case(str_plural($model_trans));
//		$create_model = 'create_' . snake_case($model) . '_table';

        /**
         * Create Model - Migration, Controller, View
         */
        if (!file_exists($request_base_path_admin)) {
            mkdir($request_base_path_admin, 0777, true);
        }
        if (!file_exists($request_base_path_frontend)) {
            mkdir($request_base_path_frontend, 0777, true);
        }
        if (!file_exists($controller_base_path_admin)) {
            mkdir($controller_base_path_admin, 0777, true);
        }
        if (!file_exists($controller_base_path_frontend)) {
            mkdir($controller_base_path_frontend, 0777, true);
        }
        if (!file_exists($view_base_path_admin)) {
            mkdir($view_base_path_admin, 0777, true);
        }
//        if (!file_exists($view_base_path_frontend)) {
//            mkdir($view_base_path_frontend, 0777, true);
//        }

        if (!file_exists($model_full_path)) {

            // Model
            $this->call('make:model', ['name' => $model_path, '-m' => true]);
			
			// Migration edit
            $arr_files = glob(database_path('migrations\*.php'));
            $migrate_files = preg_filter('~' . preg_quote("create_{$table_name}_table.php", '~') . '$~', '$0', $arr_files);
            $migrate_file = array_shift($migrate_files);
            $this->line($migrate_file);
            $migration_content = file_get_contents($migrate_file);
            $migration_content = str_replace("\$table->increments('id');\n", "\$table->increments('id');\n\n\t\t\t\$table->string('name')->nullable();\n\t\t\t\$table->string('image')->nullable();\n\t\t\t\$table->unsignedInteger('sort_order');\n\n", $migration_content);
            file_put_contents($migrate_file, $migration_content);
			
			// Model trans creation
            $this->call('make:model', ['name' => $model_trans_path, '-m' => true]);

            // Migration trans edit
            $arr_files = glob(database_path('migrations\*.php'));
            $migrate_files = preg_filter('~' . preg_quote("create_{$table_trans_name}_table.php", '~') . '$~', '$0', $arr_files);
            $migrate_file = array_shift($migrate_files);
            $this->line($migrate_file);
            $migration_content = file_get_contents($migrate_file);
            $migration_content = str_replace("\$table->bigIncrements('id');\n", "\$table->bigIncrements('id');
            
            \$table->unsignedBigInteger('$relation_id')->nullable();
            \$table->foreign('$relation_id')->references('id')->on('$table_name')->onDelete('cascade');
            
            \$table->string('name')->nullable();
            \$table->unsignedInteger('sort_order')->default(0);
            
            \App\SeoHelper\SeoMigration::columns(\$table);
            
            \$table->string('locale')->index();
            \$table->unique(['$relation_id', 'locale']);\n",
                $migration_content);
            $migration_content = str_replace("\$table->timestamps();", "", $migration_content);
            file_put_contents($migrate_file, $migration_content);

            // Request
            File::copy($template_path_request_admin, $request_full_path_admin);
            // File::copy($template_path_request_frontend, $request_full_path_frontend);

            // Controller
            File::copy($template_path_controller_admin, $controller_full_path_admin);
            // File::copy($template_path_controller_frontend, $controller_full_path_frontend);

            // View
            File::copy($template_path_view_add_admin, $view_add_full_path_admin);
            File::copy($template_path_view_edit_admin, $view_edit_full_path_admin);
            File::copy($template_path_view_list_admin, $view_list_full_path_admin);
            // File::copy($template_path_view_add_frontend, $view_add_full_path_frontend);
            // File::copy($template_path_view_edit_frontend, $view_edit_full_path_frontend);
            // File::copy($template_path_view_list_frontend, $view_list_full_path_frontend);

            /**
             * Rewrite Model, Controller, View
             */
            /**
             * Model
             */
            $model_template = File::get($template_path_model);
            $model_template = str_replace('{ModelName}', $model, $model_template);
            $model_template = str_replace('{TableName}', $table_name, $model_template);
            File::put($model_full_path, $model_template);
            $this->line('Rewrite model complete!');
			
			/**
             * Model trans
             */
            $model_template = File::get($template_path_model_trans);
            $model_template = str_replace('{ModelName}', $model_trans, $model_template);
            $model_template = str_replace('{TableName}', $table_trans_name, $model_template);
            File::put($model_trans_full_path, $model_template);
            $this->line('Rewrite model trans complete!');

            /**
             * Request Admin
             */
            $request_template_admin = File::get($template_path_request_admin);
            $request_template_admin = str_replace('{ModelName}', $model, $request_template_admin);
			$request_template_admin = str_replace('{TableName}', $table_name, $request_template_admin);
            File::put($request_full_path_admin, $request_template_admin);
            $this->line('Create admin request complete!');

            /**
             * Request Frontend
             */
            // $request_template_frontend = File::get($template_path_request_frontend);
            // $request_template_frontend = str_replace('{ModelName}', $model, $request_template_frontend);
			// $request_template_frontend = str_replace('{TableName}', $table_name, $request_template_frontend);
            // File::put($request_full_path_frontend, $request_template_frontend);
            // $this->line('Create frontend request complete!');

            /**
             * Controller Admin
             */
            $controller_template_admin = File::get($template_path_controller_admin);
            $controller_template_admin = str_replace('{ModelName}', $model, $controller_template_admin);
            $controller_template_admin = str_replace('{RouteName}', $prefix, $controller_template_admin);
            File::put($controller_full_path_admin, $controller_template_admin);
            $this->line('Create admin controller complete!');

            /**
             * Controller Frontend
             */
            // $controller_template_frontend = File::get($template_path_controller_frontend);
            // $controller_template_frontend = str_replace('{ModelName}', $model, $controller_template_frontend);
            // $controller_template_frontend = str_replace('{RouteName}', $prefix, $controller_template_frontend);
            // File::put($controller_full_path_frontend, $controller_template_frontend);
            // $this->line('Create frontend controller complete!');

            /**
             * View Admin
             */
            // Add
            $view_template_admin = File::get($template_path_view_add_admin);
            $view_template_admin = str_replace('{ModelName}', $model, $view_template_admin);
            $view_template_admin = str_replace('{RouteName}', $prefix, $view_template_admin);
            File::put($view_add_full_path_admin, $view_template_admin);
            $this->line('Create admin view add complete!');

            // Edit
            $view_template_admin = File::get($template_path_view_edit_admin);
            $view_template_admin = str_replace('{ModelName}', $model, $view_template_admin);
            File::put($view_edit_full_path_admin, $view_template_admin);
            $this->line('Create admin view edit complete!');

            // List
            $view_template_admin = File::get($template_path_view_list_admin);
            $view_template_admin = str_replace('{ModelName}', $model, $view_template_admin);
			$view_template_admin = str_replace('{RouteName}', $prefix, $view_template_admin);
            File::put($view_list_full_path_admin, $view_template_admin);
            $this->line('Create admin view list complete!');

            /**
             * View Frontend
             */
            // Add
            // $view_template_frontend = File::get($template_path_view_add_frontend);
            // $view_template_frontend = str_replace('{ModelName}', $model, $view_template_frontend);
            // $view_template_frontend = str_replace('{RouteName}', $prefix, $view_template_frontend);
            // File::put($view_add_full_path_frontend, $view_template_frontend);
            // $this->line('Create frontend view add complete!');

            // Edit
            // $view_template_frontend = File::get($template_path_view_edit_frontend);
            // $view_template_frontend = str_replace('{ModelName}', $model, $view_template_frontend);
            // File::put($view_edit_full_path_frontend, $view_template_frontend);
            // $this->line('Create frontend view edit complete!');

            // List
            // $view_template_frontend = File::get($template_path_view_list_frontend);
            // $view_template_frontend = str_replace('{ModelName}', $model, $view_template_frontend);
			// $view_template_frontend = str_replace('{RouteName}', $prefix, $view_template_frontend);
            // File::put($view_list_full_path_frontend, $view_template_frontend);
            // $this->line('Create frontend view list complete!');

            /**
             * Create route
             */

            $route_content = file_get_contents($appPath . '\Http\routes.php');
      $route =
        "Route::group(['prefix' => '{$prefix}'], function() {
            Route::get('/',            ['as' => 'admin.{$prefix}.list',    'uses' => '{$controller_name}@getList']);
            Route::post('/',           ['as' => 'admin.{$prefix}.list',    'uses' => '{$controller_name}@postList']);
            Route::get('add',          ['as' => 'admin.{$prefix}.add',     'uses' => '{$controller_name}@getAdd']);
            Route::post('add',         ['as' => 'admin.{$prefix}.add',     'uses' => '{$controller_name}@postAdd']);
            Route::get('edit/{id}',    ['as' => 'admin.{$prefix}.edit',    'uses' => '{$controller_name}@getEdit']);
            Route::post('edit/{id}',   ['as' => 'admin.{$prefix}.edit',    'uses' => '{$controller_name}@postEdit']);
            Route::post('delete/{id}', ['as' => 'admin.{$prefix}.delete',  'uses' => '{$controller_name}@postDelete']);
            Route::post('deletes',     ['as' => 'admin.{$prefix}.deletes', 'uses' => '{$controller_name}@postDeletes']);
        });";

            // Check if the inject position is exist
            if (strpos($route_content, "/** CRUD generate here, do not delete this line */") !== false) {
                $route_content = str_replace_first("/** CRUD generate here, do not delete this line */",
                    "$route\r\n
        /** CRUD generate here, do not delete this line */",
                    $route_content);
            } else {
                $route_content .= "\n$route\n";
            }
            File::put($appPath . '\Http\routes.php', $route_content);
            $this->line('Create route complete!');

            /**
             * Create menu
             */
            $menu_file = config_path('menu.php');
            $menu_content = file_get_contents($menu_file);
            $menu_content = str_replace_first(
                "'admin' => [\r\n",
                "'admin' => [
        '{$prefix}' => [
            'route'      => 'admin.{$prefix}.list',
            'label'      => '{$model}',
            'icon'       => 'icon-home',
            'menu_order' => 10
        ],\r\n",
                $menu_content);
            file_put_contents($menu_file, $menu_content);
            $this->line('Create menu complete!');
        } else {
            $this->line('Model already existed!');
        }
    }
}
