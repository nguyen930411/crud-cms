<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>@section('title'){{ getOption('title') }}@show</title>

    @yield('meta')

    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" href="{{ static_file('static/assets/js/vendor/fontawesome/css/font-awesome.min.css') }}">
    <link rel="stylesheet" href="{{ static_file('static/assets/js/vendor/bootstrap/dist/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ static_file('static/assets/css/bootstrap-social.css') }}">

    <style>
        body {
            padding-top: 50px;
            /*padding-bottom: 20px;*/
        }
    </style>
    <link rel="stylesheet" href="{{ static_file('static/assets/js/vendor/bootstrap/dist/css/bootstrap-theme.min.css') }}">
    <link rel="stylesheet" href="{{ static_file('static/assets/js/vendor/seiyria-bootstrap-slider/dist/css/bootstrap-slider.min.css') }}">
    <link rel="stylesheet" href="{{ static_file('static/global/plugins/bootstrap-datepicker/css/datepicker.css') }}">
    <link rel="stylesheet" href="{{ static_file('static/global/plugins/joyride-master/joyride-2.1.css') }}">
    <link rel="stylesheet" href="{{ static_file('static/assets/js/vendor/select2/css/select2.min.css') }}">
    <link rel="stylesheet" href="{{ static_file('static/assets/js/vendor/owl.carousel.2/assets/owl.carousel.css') }}">
    <link rel="stylesheet" href="{{ static_file('static/assets/css/main.css') }}">
    <link rel="stylesheet" href="{{ static_file('static/assets/css/style.min.css') }}">

    <script src="{{ static_file('static/assets/js/vendor/modernizr-2.8.3-respond-1.4.2.min.js') }}"></script>
    <script>
        var basePath = "{{ url('/') }}";
        var uploader_config = {
            url : '<?php echo e(route('file.add')); ?>',
            flash_swf_url : '<?php echo e(static_file('static/plugins/plupload/js/Moxie.swf')); ?>',
            silverlight_xap_url : '<?php echo e(static_file('static/plugins/plupload/js/Moxie.xap')); ?>',
            _token : '<?php echo e(csrf_token()); ?>',
            delete_url : '<?php echo e(route('file.delete')); ?>'
        }
        var pc = "user_{{ Auth::id() }}";
    </script>

    @yield('styles')
</head>
<body>
<!-- Load Facebook SDK for JavaScript -->
<div id="fb-root"></div>
<script>(function (d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s);
        js.id = id;
        js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
</script>

<!--[if lt IE 8]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
<![endif]-->
<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="{{ url('/') }}">
                <img src="{{ static_file('static/assets/img/vworkstop.png') }}">
            </a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav navbar-right">
                {{--<li><a href="{{ route('frontend.add-product.product-specs') }}">Add Product</a></li>--}}
                {{--<li><a href="{{ url('portfolio') }}">PORTFOLIO</a></li>--}}
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">BLOGS <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="{{ route('frontend.blog.list') }}">All Blogs</a></li>
                        <li><a href="{{ route('frontend.blog.my-blog') }}">My Blogs <span class="badge badge-my-blog">{{ $my_blog_count or '' }}</span></a></li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">ARTISTS <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        @foreach ($group_artists as $art)
                            <li><a href="{{ route('frontend.artist', ['slug' => str_slug($art->name, '-'), 'id' => $art->id]) }}">{{ $art->name }}</a></li>
                        @endforeach
                    </ul>
                </li>
                <li>
                    <a href="{{ route('frontend.cart.checkout') }}">
                        <i class="fa fa-shopping-cart"></i> CART
                        @if($cart_count = Cart::instance('shopping')->count())
                            <span class="badge">{{ $cart_count }}</span>
                        @endif
                    </a>
                </li>
                @if(Auth::check())
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                            @if (Auth::user()->user_class)
                                <img width="20" src="{{ static_file(Auth::user()->user_class->icon) }}" data-toggle="tooltip" title="{{ Auth::user()->user_class->name }}" data-placement="left">
                            @endif
							@if (Auth::user()->verification)
								<i data-toggle="tooltip" title="Verified" class="fa fa-check-circle-o verified" data-placement="left"> </i>
							@endif
                            @if (Auth::user()->profile->avatar)
                                <img src="{{ static_file(Auth::user()->profile->avatar) }}" width="30">
                            @else
                                    <i class="fa fa-user"> </i>
                            @endif
                                {{ Auth::user()->profile->name ?: Auth::user()->name }}
                            @if($notify = Auth::user()->notifications()->where('seen', 0)->count())
                                <span class="badge">{{ $notify }}</span>
                            @endif
                            <span class="caret"></span>
                        </a>

                        @if (Auth::user()->star > 0)
                            <a href="{{ route('frontend.my-rating.list') }}" style="text-align: center; margin-top: -20px; padding: 0;" data-toggle="tooltip" title="{{ Auth::user()->star }}" data-placement="bottom">
                                <span class="starprof" data-score="{{ Auth::user()->star }}"></span>
                            </a>
                        @endif

                        <ul class="dropdown-menu">
                            <li><a href="{{ route('frontend.notifications.list') }}">Notifications <span class="badge badge-success">{{ $notify }}</span></a></li>
                            <li><a href="{{ route('frontend.balance.history') }}">Balance <span class="badge">{{ Auth::user()->balance }}</span></a></li>
                            <li><a href="{{ route('frontend.my-project.add') }}">Create Project</a></li>
                            <li><a href="{{ route('frontend.my-project.list') }}">My Project</a></li>
                            <li><a href="{{ route('frontend.order.history') }}">Order History</a></li>
                            <li><a href="{{ route('frontend.user-sales.list') }}">My Sales</a></li>
                            @if (!empty(Auth::user()->group_artist_id))
                                <li><a href="{{ route('frontend.uploaded-product.list') }}">My Products</a></li>
                                <li><a href="{{ route('frontend.add-product.product-specs') }}">Publish Products</a></li>
                                <li><a href="{{ route('frontend.add-product.product-specs', ['portfolio' => 1]) }}">Upload Product Portfolio</a></li>
                            @endif
                            <li><a href="{{ route('frontend.account.artist.index') }}">Account</a></li>
                            <li><a href="{{ route('frontend.dispute.list') }}">My Dispute <span class="badge badge-danger">{{ $count_dispute }}</span></a></li>
                            <li><a href="{{ route('frontend.invite') }}"><i class="fa fa-user-plus"></i> Invite Friends</a></li>
                            <li><a href="{{ url('logout') }}"><i class="fa fa-sign-out"></i> LOGOUT</a></li>
                        </ul>
                    </li>
                @else
                    <li><a href="{{ url('sign-in') }}"><i class="fa fa-user"></i> SIGN IN / JOIN</a></li>
                @endif

                @yield('hire')
            </ul>
        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</nav>

@if (Session::has('flash_message'))
    <div class="flash-message">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    @include('blocks.flash-message')
                </div>
            </div>
        </div>
    </div>
@endif

@yield('content')

<footer id="#copyright_part" class="thefooter">
    <div class="container">
        <div class="row">
            <div class=col-md-1></div>
            <div class=col-md-1>
                <!-- (c) 2005, 2016. Authorize.Net is a registered trademark of CyberSource Corporation -->
                <div class="AuthorizeNetSeal">
                    <script type="text/javascript" language="javascript">var ANS_customer_id = "84f99a6b-aaa2-4467-a036-222dec7fb388";</script>
                    <script type="text/javascript" language="javascript" src="//verify.authorize.net/anetseal/seal.js"></script>
                    <a href="http://www.authorize.net/" id="AuthorizeNetText" target="_blank">Online Payments</a>
                </div>
            </div>
            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                <div class="footer-menu">
                    @foreach($pages as $key => $page)
                        <a href="{{ route('frontend.page', ['slug' => str_slug($page->name), 'id' => $page->id]) }}">{{ $page->title }}</a> {{ $key + 1 == count($pages) ? '' : '|' }}
                    @endforeach
                </div>
                <div class="copyright">&copy; 2015 VizualWorks All rights reserved</div>
            </div>

            <div class="socials align-right col-xs-6 col-sm-6 col-md-6 col-lg-6">
                <ul class="list-inline">
                    <li data-toggle="tooltip" title="Facebook"><a target="_blank" rel="nofollow" href="{{ getOption('facebook', '#') }}"><i class="fa fa-facebook"></i></a></li>
                    <li data-toggle="tooltip" title="Twitter"><a target="_blank" rel="nofollow" href="{{ getOption('twitter', '#') }}"><i class="fa fa-twitter"></i></a></li>
                    <li data-toggle="tooltip" title="Google"><a target="_blank" rel="nofollow" href="{{ getOption('google', '#') }}"><i class="fa fa-google-plus"></i></a></li>
                    <li data-toggle="tooltip" title="LinkedIn"><a target="_blank" rel="nofollow" href="{{ getOption('linkedin', '#') }}"><i class="fa fa-linkedin"></i></a></li>
                    <li data-toggle="tooltip" title="Youtube"><a target="_blank" rel="nofollow" href="{{ getOption('youtube', '#') }}"><i class="fa fa-youtube"></i></a></li>
                    <li data-toggle="tooltip" title="Instagram"><a target="_blank" rel="nofollow" href="{{ getOption('instagram', '#') }}"><i class="fa fa-instagram"></i></a></li>
                    <li data-toggle="tooltip" title="Pinterest"><a target="_blank" rel="nofollow" href="{{ getOption('pinterest', '#') }}"><i class="fa fa-pinterest"></i></a></li>
                </ul>
            </div>
        </div>
    </div>
</footer>
<div class="modal-loading"></div>

<!-- SiteLock -->
<div style="position: fixed; z-index: 1000; width: 117px; height: 67px; bottom: 15px; left: 15px;"><a href="#" onclick="window.open('https://www.sitelock.com/verify.php?site=vizualworks.com','SiteLock','width=600,height=600,left=160,top=170');" ><img class="img-responsive" alt="SiteLock" title="SiteLock" src="//shield.sitelock.com/shield/vizualworks.com" /></a></div>

<script src="{{ static_file('static/assets/js/vendor/jquery/dist/jquery.min.js') }}"></script>
{{--<script src="{{ static_file('static/global/plugins/jquery-ui/jquery-ui-1.10.3.custom.min.js') }}"></script>--}}
<script src="{{ static_file('static/assets/js/vendor/bootstrap/dist/js/bootstrap.min.js') }}"></script>
<script src="{{ static_file('static/plugins/plupload/js/plupload.full.min.js') }}"></script>
<script src="{{ static_file('static/assets/js/vendor/jquery-ui/ui/minified/datepicker.min.js') }}"></script>
<script src="{{ static_file('static/assets/js/vendor/seiyria-bootstrap-slider/dist/bootstrap-slider.min.js') }}"></script>
<script src="{{ static_file('static/assets/js/vendor/raty/lib/jquery.raty.js') }}"></script>
<script src="{{ static_file('static/assets/js/vendor/select2/js/select2.min.js') }}"></script>
<script src="{{ static_file('static/assets/js/vendor/owl.carousel.2/owl.carousel.min.js') }}"></script>
<script src="{{ static_file('static/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js') }}"></script>
<script src="{{ static_file('static/global/plugins/uniform/jquery.uniform.min.js') }}" type="text/javascript"></script>
<script src="{{ static_file('static/global/plugins/joyride-master/jquery.cookie.js') }}" type="text/javascript"></script>
<script src="{{ static_file('static/global/plugins/joyride-master/jquery.joyride-2.1.js') }}" type="text/javascript"></script>
<script src="{{ static_file('static/global/plugins/ckeditor/ckeditor.js') }}" type="text/javascript"></script>
<script src="{{ static_file('static/global/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js') }}"></script>

<script src="https://js.pusher.com/3.1/pusher.min.js"></script>
<script src="{{ static_file('static/assets/js/main.js') }}"></script>

{{--Custom Lib--}}
<script src="{{ static_file('static/global/plugins/bootbox/bootbox.min.js') }}" type="text/javascript"></script>

{{--LinkedIn--}}
<script src="//platform.linkedin.com/in.js" type="text/javascript"> lang: en_US</script>

{{--Tweet--}}
<script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>

<audio id="notifySound">
    <source src="{{ static_file('static/notify/notify.ogg') }}" type="audio/ogg">
    <source src="{{ static_file('static/notify/notify.mp3') }}" type="audio/mpeg">
    <source src="{{ static_file('static/notify/notify.wav') }}" type="audio/wav">
</audio>

<script>
    $(document).ready(function () {
        $('.starprof').raty({
            number: 5,
            halfShow: false,
            starOn: '{{ static_file('static/assets/js/vendor/raty/lib/images/star-on.png') }}',
            starOff: '{{ static_file('static/assets/js/vendor/raty/lib/images/star-off.png') }}',
            score: function() {
                return $(this).attr('data-score');
            },
            readOnly: true,
            hints: ['','','','','']
        });

        // Max length
        $('.has-maxlength').maxlength({
            alwaysShow: true
        });
    });

    // Loading gif
    $body = $("body");
    $(document).on({
        ajaxStart: function() { $body.addClass("loading"); },
        ajaxStop: function() { $body.removeClass("loading"); }
    });
    $(window).bind('beforeunload', function(){
        $body.addClass("loading");
    });
</script>

@yield('script')

{!! getOption('end_content') !!}

</body>
</html>
