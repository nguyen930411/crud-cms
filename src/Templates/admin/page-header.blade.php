<div class="page-bar">
    {!! Breadcrumb::render() !!}

    @if(isset($actions) && $actions)
    <div class="page-toolbar">
        <div class="btn-group pull-right">
            <button type="button" class="btn btn-fit-height grey-salt dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="1000" data-close-others="true">
                Actions <i class="fa fa-angle-down"></i>
            </button>
            <ul class="dropdown-menu pull-right" role="menu">
                @foreach($actions as $action)
                <li>
                    <a href="{{ $action['href'] }}" class="{{ $action['class'] or '' }}" {!! get_attrs_action($action) !!}>
                        @if(isset($action['icon']))
                            <i class="{{ $action['icon'] }}"></i>
                        @endif
                        {{ $action['label'] }}
                    </a>
                </li>
                @endforeach
            </ul>
        </div>
    </div>
    @endif
</div>