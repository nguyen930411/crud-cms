<li class="{{ isMenuGroupActive('admin.dashboard.index') }}">
    <a href="{{ route('admin.dashboard.index') }}">
        <i class="icon-home"></i>
        <span class="title">{{ trans('dashboard.menu-title') }}</span>
    </a>
</li>
@foreach(config('menu.admin', []) as $item)
    @if(can_access_menu($item))
        <li class="{{ menu_active_class($item) }}">
            <a href="{{ route($item['route']) }}">
                @if(!empty($item['icon']))
                    <i class="{{ $item['icon'] }}"></i>
                @endif

                @if(!empty($item['label']))
                    <span class="title">{{ $item['label'] }}</span>
                @elseif(!empty($item['tran']))
                    <span class="title">{{ trans($item['tran']) }}</span>
                @endif

                @if(!empty($item['child']))
                    <span class="arrow"></span>
                @endif
            </a>

            @if(isset($item['child']) && is_array($item['child']))
                <ul class="sub-menu">
                    @foreach($item['child'] as $child)
                        @if(can_access_menu($child))
                            <li class="{{ isMenuActive($child['route']) }}">
                                <a href="{{ route($child['route']) }}">
                                    @if(!empty($child['icon']))
                                        <i class="{{ $child['icon'] }}"></i>
                                    @endif

                                    @if(!empty($child['label']))
                                        <span class="title">{{ $child['label'] }}</span>
                                    @elseif(!empty($child['tran']))
                                        <span class="title">{{ trans($child['tran']) }}</span>
                                    @endif
                                </a>
                            </li>
                        @endif
                    @endforeach
                </ul>
            @endif
    </li>
    @endif
@endforeach