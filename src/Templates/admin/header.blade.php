<div class="page-header navbar navbar-fixed-top">
    <div class="page-header-inner">
        <a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="glyphicon glyphicon-align-justify" style="color: #ffffff;font-size: 20px;"></span>
        </a>

        <div class="page-logo">
            <a href="{{ url('/') }}" target="_blank" title="Home Page" class="text-center" style="width: 100%;">
                <img style="margin: 2px; height: 40px;" src="{{ static_file(getOption('logo_admin', 'static/admin/img/logo.png')) }}" alt="logo" class="logo-default"/>
            </a>
        </div>

        <div class="top-menu">
            <ul class="nav navbar-nav pull-right">
                <li class="dropdown dropdown-user">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                        <span class="username">{{ Auth::user()->name }}</span>
                        <i class="fa fa-angle-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-menu-default">
                        <li>
                            <a href="{{ route('admin.profile.index') }}">
                                <i class="icon-user"></i>
                                {{ trans('user.profile') }}
                            </a>
                        </li>
                        <li>
                            <a href="{{ route('admin.profile.password') }}">
                                <i class="icon-lock"></i>
                                {{ trans('user.change_pass') }}
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="{{ url('admin/logout') }}" class="no-pjax">
                                <i class="icon-key"></i> Logout
                            </a>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</div>