<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" class="no-js" ng-app="BaseCMSApp">
<!--<![endif]-->

    <head>
        <meta charset="utf-8"/>
        @if(isset($title) && $title)
            <title>{{ $title }} - {{ getOption('title') }}</title>
        @else
            <title>@yield('title') - {{ getOption('title') }}</title>
        @endif
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport"/>

        <link href="//fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css"/>
        <link href="{{ static_file('static/admin/global/plugins/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css"/>
        <link href="{{ static_file('static/admin/global/plugins/simple-line-icons/simple-line-icons.min.css') }}" rel="stylesheet" type="text/css"/>
        <link href="{{ static_file('static/admin/global/plugins/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css"/>
        <link href="{{ static_file('static/admin/global/plugins/uniform/css/uniform.default.css') }}" rel="stylesheet" type="text/css"/>
        <link href="{{ static_file('static/admin/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css') }}" rel="stylesheet" type="text/css"/>
        <link href="{{ static_file('static/admin/global/plugins/select2/select2.css') }}" rel="stylesheet" type="text/css"/>
        <link href="{{ static_file('static/admin/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css') }}" rel="stylesheet" type="text/css"/>
        <link href="{{ static_file('static/admin/global/plugins/jquery-tags-input/jquery.tagsinput.css') }}" rel="stylesheet" type="text/css"/>
        <link href="{{ static_file('static/admin/global/plugins/bootstrap-toastr/toastr.min.css') }}" rel="stylesheet" type="text/css"/>
        <link href="{{ static_file('static/admin/global/plugins/bootstrap-select/bootstrap-select.min.css') }}" rel="stylesheet" type="text/css"/>
        <link href="{{ static_file('static/admin/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css') }}" rel="stylesheet" type="text/css"/>
        <link href="{{ static_file('static/admin/includes/jquery.datetimepicker.css') }}" rel="stylesheet" type="text/css"/>

        <link href="{{ static_file('static/admin/global/css/components.min.css') }}" id="style_components" rel="stylesheet" type="text/css"/>
        <link href="{{ static_file('static/admin/global/css/plugins.min.css') }}" rel="stylesheet" type="text/css"/>
        <link href="{{ static_file('static/admin/layout/css/layout.min.css') }}" rel="stylesheet" type="text/css"/>
        <link href="{{ static_file('static/admin/layout/css/themes/darkblue.css') }}" rel="stylesheet" type="text/css" id="style_color"/>
        <link href="{{ static_file('static/admin/pages/css/timeline.css') }}" rel="stylesheet" type="text/css"/>

        {{-- Custom style --}}
        <link href="{{ static_file('static/admin/style.min.css') }}" rel="stylesheet" type="text/css"/>

        @yield('styles')

        {{-- jQuery --}}
        <script src="{{ static_file('static/admin/global/plugins/jquery.min.js') }}" type="text/javascript"></script>

        <link rel="shortcut icon" href="{{ static_file('favicon.ico') }}"/>

        <script>
            var basePath = '{{ url('/') }}';
        </script>
    </head>

    <body class="page-header-fixed page-quick-sidebar-over-content page-style-square">
        @include('admin.header')

        <div class="clearfix"></div>

        <div class="page-container">
            @include('admin.sidebar')

            @section('main-content')
                <div class="page-content-wrapper">
                    <div class="page-content" id="pjax-container">
                        @include('admin.page-header')

                        @include('blocks.flash-message')
                        @include('blocks.error')

                        @yield('content')
                    </div>
                </div>
            @show
        </div>

        @include('admin.footer')

        <script src="{{ static_file('static/admin/global/plugins/jquery-migrate.min.js') }}" type="text/javascript"></script>
        <script src="{{ static_file('static/admin/global/plugins/jquery-ui/jquery-ui-1.10.3.custom.min.js') }}" type="text/javascript"></script>
        <script src="{{ static_file('static/admin/global/plugins/bootstrap/js/bootstrap.min.js') }}" type="text/javascript"></script>
        <script src="{{ static_file('static/admin/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js') }}" type="text/javascript"></script>
        <script src="{{ static_file('static/admin/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js') }}" type="text/javascript"></script>
        <script src="{{ static_file('static/admin/global/plugins/jquery.blockui.min.js') }}" type="text/javascript"></script>
        <script src="{{ static_file('static/admin/global/plugins/uniform/jquery.uniform.min.js') }}" type="text/javascript"></script>
        <script src="{{ static_file('static/admin/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js') }}" type="text/javascript"></script>

        <script src="{{ static_file('static/admin/global/scripts/metronic.js') }}" type="text/javascript"></script>
        <script src="{{ static_file('static/admin/layout/scripts/layout.js') }}" type="text/javascript"></script>

        <script>
            jQuery(document).ready(function() {
                Metronic.init();
                Layout.init();
            });
        </script>

        {{-- Custom lib --}}
        <script src="{{ static_file('static/admin/global/plugins/bootbox/bootbox.min.js') }}" type="text/javascript"></script>
        <script src="{{ static_file('static/admin/global/plugins/bootstrap-toastr/toastr.min.js') }}" type="text/javascript"></script>
        <script src="{{ static_file('static/admin/global/plugins/bootstrap-select/bootstrap-select.min.js') }}" type="text/javascript"></script>
        <script src="{{ static_file('static/admin/global/plugins/select2/select2.min.js') }}" type="text/javascript"></script>
        <script src="{{ static_file('static/admin/global/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js') }}" type="text/javascript"></script>
        <script src="{{ static_file('static/admin/global/plugins/ckeditor/ckeditor.js') }}" type="text/javascript"></script>
        <script src="{{ static_file('static/admin/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js') }}" type="text/javascript"></script>
        <script src="{{ static_file('static/admin/global/plugins/jquery-tags-input/jquery.tagsinput.min.js') }}" type="text/javascript"></script>
        {{--<script src="{{ static_file('static/admin/plugins/plupload/js/plupload.full.min.js') }}"></script>--}}
        <script src="{{ static_file('static/admin/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js') }}"></script>
        <script src="{{ static_file('static/admin/global/plugins/counterup/jquery.waypoints.min.js') }}"></script>
        <script src="{{ static_file('static/admin/global/plugins/counterup/jquery.counterup.min.js') }}"></script>
        <script src="{{ static_file('static/admin/global/plugins/datatables/media/js/jquery.dataTables.min.js') }}"></script>
        <script src="{{ static_file('static/admin/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js') }}"></script>
        <script src="{{ static_file('static/admin/global/plugins/moment-with-locales.js') }}"></script>
        <script src="{{ static_file('static/admin/global/plugins/jquery.infinitescroll.min.js') }}"></script>

        {{-- Include lib --}}
        <script src="{{ static_file('static/admin/includes/jquery.pjax.min.js') }}" type="text/javascript"></script>
        <script src="{{ static_file('static/admin/includes/underscore-min.js') }}" type="text/javascript"></script>
        <script src="{{ static_file('static/admin/includes/jquery.datetimepicker.full.min.js') }}" type="text/javascript"></script>

        <audio id="notifySound">
            <source src="{{ static_file('static/admin/notify/notify.ogg') }}" type="audio/ogg">
            <source src="{{ static_file('static/admin/notify/notify.mp3') }}" type="audio/mpeg">
            <source src="{{ static_file('static/admin/notify/notify.wav') }}" type="audio/wav">
        </audio>

        {{-- Custom Script --}}
        <script>
            var mysocket = {
                url: '{{ env('SOCKET_URL') }}',
                _token: '{{ get_jwt() }}'
            }
        </script>
        <script src="{{ static_file('static/admin/script.min.js?v=1.0.1') }}" type="text/javascript"></script>

        {{-- Angular --}}
        <script src="{{ static_file('static/admin/includes/angular.min.js') }}" type="text/javascript"></script>
        <script src="{{ static_file('static/admin/includes/angular-animate.min.js') }}" type="text/javascript"></script>
        <script src="{{ static_file('static/admin/includes/ui-bootstrap-tpls.min.js') }}" type="text/javascript"></script>
        <script src="{{ static_file('static/admin/angular.min.js?v=1.0.1') }}" type="text/javascript"></script>

        @yield('scripts')
    </body>
</html>