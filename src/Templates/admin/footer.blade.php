<div class="page-footer">
    <div class="page-footer-inner">
        2015 &copy; <a href="http://websoftseo.com" target="_blank">WebSoftSeo</a>.
    </div>
    <div class="scroll-to-top">
        <i class="icon-arrow-up"></i>
    </div>
</div>