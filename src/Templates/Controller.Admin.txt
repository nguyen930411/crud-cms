<?php

namespace App\Http\Controllers\Admin;

use App\Models\{ModelName};
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\{ModelName}Request;
use Breadcrumb;
use Auth;

class {ModelName}Controller extends Controller
{
    protected $title = '{ModelName}';

    public function __construct()
    {
        Breadcrumb::add('Dashboard', route('admin.dashboard.index'))
            ->add($this->title, route('admin.{RouteName}.list'));
    }

    public function getList(Request $request)
    {
        $items = {ModelName}::orderBy($request->input('order', 'id'), $request->input('order_by', 'DESC'));

        // Search
        if ($search = $request->input('name')) {
            $items->where('name', 'like', "%{$search}%");
        }
        if ($search = $request->input('created_at_start')) {
            $items->where('created_at', '>=', $search . ' 00:00:00');
        }
        if ($search = $request->input('created_at_end')) {
            $items->where('created_at', '<=', $search . ' 23:59:59');
        }

		/**
         * Export to excel
         */
//        if ($export = $request->export) {
//            $items = $items->get();
//
//            $header = [
//                'STT',
//                'Họ tên',
//                'Ngày tạo',
//            ];
//
//            $data = array();
//            foreach ($items as $key => $item) {
//                $data[$key][] = $key + 1;
//                $data[$key][] = $item->name ? : '';
//                $data[$key][] = $item->created_at->format('d/m/Y H:i:s');
//            }
//
//            exportExcel('Tên file', $header, $data);
//        }
		
        $items = $items->paginate($request->input('limit', 20));
        $paginate_data = $request->except(['_pjax']);

        $title = $this->title;

        $actions = [];
        if (user_can('admin.{RouteName}.add')) {
            $actions[] = [
                'href'  => route('admin.{RouteName}.add'),
                'icon'  => 'fa fa-plus',
                'label' => trans('action.add'),
                'class' => 'btn btn-success'
            ];
        }
        if (user_can('admin.{RouteName}.deletes')) {
            $actions[] = [
                'href'  => route('admin.{RouteName}.deletes'),
                'label' => trans('action.deletes'),
                'icon'  => 'fa fa-trash-o',
                'class' => 'confirm-deletes btn btn-danger',
                'attrs' => [
                    'data-message' => trans('action.deletes_confirm')
                ]
            ];
        }

        return view('admin.{RouteName}.list', compact('items', 'paginate_data', 'actions', 'title'));
    }

	/**
     * Sort order function
     */
//    public function postList(Request $request)
//    {
//        $sort = sortRow({ModelName}::class, $request->input('sort'), $request->input('item_ids'), $request->input('sort_move', 'up'));
//        if ($sort) {
//            return back()->with([
//                'flash_level'   => 'success',
//                'flash_message' => trans('action.update_success')
//            ]);
//        } else {
//            return back();
//        }
//    }

    public function getAdd()
    {
        $title = ucfirst(trans('action.add') . ' ' . $this->title);
        Breadcrumb::add($title, route('admin.{RouteName}.add'));

        return view('admin.{RouteName}.add', compact('title'));
    }

    public function postAdd({ModelName}Request $request)
    {
        $data = $request->except(['_pjax', '_token']);

        $data['image'] = $request->file('image');
//        $data['images'] = implode(',', $data['images']);

        $item = {ModelName}::create($data);
		
		$item->update(['sort_order' => {ModelName}::max('sort_order') + 1]);

        return redirect()->route('admin.{RouteName}.list')->with([
            'flash_level'   => 'success',
            'flash_message' => trans('action.add_success')
        ]);
    }

    public function getEdit($id)
    {
		set_previous_url();
		
        $item = {ModelName}::where('id', $id)->first();
        if (!$item) {
            return redirect()->route('admin.{RouteName}.list');
        }

        $title = ucfirst(trans('action.update') . ' ' . $this->title);
        Breadcrumb::add($title, route('admin.{RouteName}.edit', $id));

        return view('admin.{RouteName}.edit', compact('item', 'title'));
    }

    public function postEdit($id, {ModelName}Request $request)
    {
        $item = {ModelName}::find($id);
        $data = $request->except(['_pjax', '_token']);

        if ($item) {
            if ($request->hasFile('image')) {
                $data['image'] = $request->file('image');
            } else {
				unset($data['image']);
            }
//            $data['images'] = implode(',', $data['images']);

			$item->update($data);

            return redirect(get_previous_url(route('admin.{RouteName}.list')))->with([
                'flash_level'   => 'success',
                'flash_message' => trans('action.update_success')
            ]);
        } else {
            return redirect(get_previous_url(route('admin.{RouteName}.list')));
        }
    }

    public function postDelete($id)
    {
		$item = {ModelName}::find($id);
		if ($item) {
			$item->delete();
			
			return back()->with([
				'flash_level'   => 'danger',
				'flash_message' => trans('action.deleted', ['name' => $item->name]),
			]);
		} else {
			return back()->with([
				'flash_level'   => 'danger',
				'flash_message' => trans('action.cant_delete'),
			]);
		}
    }

    public function postDeletes(Request $request)
    {
        if (!$request->input('ids')) {
            return back()->with([
                'flash_level'   => 'danger',
                'flash_message' => trans('action.not_items')
            ]);
        }

        $ids = $request->input('ids');

		try {
			{ModelName}::destroy($ids);
        } catch (\Exception $e) {
            return back()->with([
                'flash_level'   => 'danger',
                'flash_message' => trans('action.cant_delete_foreign')
            ]);
        }

        return back()->with([
            'flash_level'   => 'success',
            'flash_message' => trans('action.items_deleted')
        ]);
    }
}
