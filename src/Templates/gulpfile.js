var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function(mix) {
    mix.scripts([
        'admin/script.js',
        'admin/nktApp.js',
        'admin/pjax.js',
        'admin/ckeditor.js',
        'admin/table-has-checkbox.js',
        'admin/tagging.js',
        'admin/socket.io.js',
        'admin/notify.js',
        'admin/notify-app.js'
    ], 'public/static/admin/script.min.js');

    mix.scripts([
        'admin/angular/checklist-model.js',
        'admin/angular/angular-dragdrop.min.js',
        'admin/angular/app.js',
        'admin/angular/zoneController.js',
        'admin/angular/ProductShopifyController.js',
        'admin/angular/vn_currency.js'
    ], 'public/static/admin/angular.min.js');

    mix.sass([
        'admin/style.scss'
    ], 'public/static/admin/style.min.css');
});
